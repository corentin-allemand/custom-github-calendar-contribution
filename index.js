import fs from "fs";
import exec from "shell-exec";

/*
const simpleGit = require('simple-git')('repos');

const USER = 'DrCoc';
const PASS = 'Zfipn73xwer55';
const REPO = 'github.com/DrCoc/my-message-repo';

const git = require('simple-git/promise');
const remote = `https://${USER}:${PASS}@${REPO}`;

// git().silent(true)
//     .clone(remote)
//     .then(() => console.log('finished'))
//     .catch((err) => console.error('failed: ', err));

simpleGit
    .add('./!*')
    .commit("first commit!")
    .addRemote('origin', remote)
    .push(['-u', 'origin', 'master'], () => console.log('done'));*/


const display = {
    "start": "08-07-2018",
    "points": [
        "21-08-2018",
        "28-08-2018",
        "04-09-2018",
        "15-08-2018",
        "12-09-2018",
        "16-08-2018",
        "13-09-2018",
        "17-08-2018",
        "24-08-2018",
        "31-08-2018",
        "07-09-2018",
        "14-09-2018",
        "18-08-2018",
        "15-09-2018",
        "19-08-2018",
        "16-09-2018",
        "20-08-2018",
        "17-09-2018",
        "21-09-2018",
        "28-09-2018",
        "05-10-2018",
        "12-10-2018",
        "19-10-2018",
        "26-10-2018",
        "02-11-2018",
        "09-11-2018",
        "16-11-2018",
        "23-11-2018",
        "30-11-2018",
        "07-12-2018",
        "14-12-2018",
        "21-12-2018",
        "28-12-2018",
        "04-01-2019",
        "11-01-2019",
        "18-01-2019",
        "25-01-2019",
        "01-02-2019",
        "08-02-2019",
        "15-02-2019",
        "22-02-2019",
        "01-03-2019",
        "08-03-2019",
        "15-03-2019",
        "22-03-2019",
        "29-03-2019",
        "05-04-2019",
        "12-04-2019",
        "19-04-2019",
        "26-04-2019",
        "03-05-2019",
        "10-05-2019",
        "17-05-2019",
        "24-05-2019",
        "31-05-2019",
        "07-06-2019",
        "14-06-2019",
        "21-06-2019"
    ]
};

async function writeCommits() {
    await display.points.forEach(async (p) => {
        fs.writeFileSync("custom-calendar-contribution.log", p + "\n", {'flag':'a'});
        await exec("git add custom-calendar-contribution.log");
        await exec("GIT_AUTHOR_DATE='" + p + "' GIT_COMMITTER_DATE='" + p + "' git commit -m '" + p + "'");
    });
}

writeCommits();